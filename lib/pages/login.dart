import 'package:flutter/material.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: [
            const SizedBox(
              height: 50,
            ),
            Image.asset("assets/images/ventas.jpg"),
            const SizedBox(
              height: 50,
            ),
            Flexible(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    const TextField(
                      obscureText: false,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'Correo Electrónico',
                      ),
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    const TextField(
                      obscureText: true,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'Contraseña',
                      ),
                    ),
                    const SizedBox(
                      height: 50,
                    ),
                    ElevatedButton(
                      child: const Text("Iniciar sesion"),
                      onPressed: () {},
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    const Text("O iniciar sesion con"),
                    const SizedBox(
                      height: 16,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        ElevatedButton(
                          child: const Text("Facebook"),
                          onPressed: () {},
                        ),
                        ElevatedButton(
                          child: const Text("Google"),
                          onPressed: () {},
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 24,
                    ),
                    TextButton(
                      child: const Text("No tiene usuario, Registrate aqui"),
                      onPressed: () {},
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
